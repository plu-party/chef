-----------------------------------------------------------------------------------------
--
-- main_Menu.lua
-- This file creates/loads the main screen for the app. This is where the timer and buttons are
--
-----------------------------------------------------------------------------------------

-- utilize the Composer library in this Lua file (must be used in every Composer scene)
local composer = require( "composer" )

--  Holds a Composer scene object
local scene = composer.newScene()

-- utilize the Widget Library in this Lua file (similar to importing a class)
local widget = require( "widget" )

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------  
--- 								LOCAL VARIABLES 								  ---
-----------------------------------------------------------------------------------------  

-- This is where we would have the goToScene() methods when needed


local width = display.contentWidth
local height = display.contentHeight
local centerX = display.contentCenterX
local centerY = display.contentCenterY

-- Forward Declarations
local backGroup
local mainGroup
local uiGroup

local background
local canvas

local timeSlider 
local clockText
local timeDisplay
local countingDownBoolean = false -- This boolean is used to check if the timer is already started/paused

-- Forward declaration of functions
local onOptions
local goToAvatar

local foodImage_base

local pan
local panEgg
local pauseButton, startButton, recipeButton, avatarButton, cancelButton
local foodTransition 

-- This will run the timer
local  countDownTimer

-- Keep track of time in seconds (5 min * 60 sec = 300 seconds)
local secondsLeft = 10 * 60

-----------------------------------------------------------------------------------------  
--- 							TIMER VARIABLES AND FUNCTIONS 						  ---
-----------------------------------------------------------------------------------------  

-- This function displays the timer in a clock format using the users input
-- Does this function only get called
local function convertToDisplayTime(time_in_seconds)
	-- time is tracked in seconds.  We need to convert it to minutes and seconds
	local minutes = math.floor( time_in_seconds / 60 )

	-- do we need this if it is always in five minute intervals?
	local seconds = time_in_seconds % 60

	-- make it a string using string format.
	timeDisplay =  string.format("%02d:%02d", minutes, seconds)
	return timeDisplay
end

-- This function determines how much time is left on the timer
local function updateTime()

		--this is where I could enter possible

      -- decrement the number of seconds
      secondsLeft = secondsLeft - 1

    if (secondsLeft > -1) then
		clockText.text = convertToDisplayTime(secondsLeft)
    end

    -- run the timer
    countDownTimer = timer.performWithDelay( 1000, updateTime, 1 )

		-- So right now nothing happens when the timer ends?


end

-- This method starts the imgage transition from beginning to end of recipe
local function startTransition()
	-- convert seconds left back to milliseconds for the transition parameter
	-- local transitionTimeConversion = math.floor(secondsLeft * 1000)

	if (foodTransition == nil) then
		-- convenience function offered
		transition.dissolve(pan, panEgg, 60 * 1000)
		foodTransition = true
	else
		transition.resume()
		print("testing")
	end

	
end

local function startTime()
	if (countingDownBoolean == true) then
		return
	end

	countingDownBoolean = true
	startButton.isVisible = false
	pauseButton.isVisible = false
	timeSlider.isVisible = false
	cancelButton.isVisible = true

	updateTime()
	startTransition()
end

-- This function pauses the timer
local function pauseTime()
	if (countingDownBoolean == false) then
		return
	end

	countingDownBoolean = false
	startButton.isVisible = true
	pauseButton.isVisible = false
	cancelButton.isVisible = false

	
	timer.pause(countDownTimer)
	transition.pause() -- cancel al transitions when paused
end

-- This function should ADDS 5 min from the timer each button press or slider notch
local function addTime(event)

	local phase = event.phase
    -- The slider is moved
	if ( "moved" == phase ) then

		-- Move the slider to the new touch position
		local mintime = 600 -- 10 min * 60 sec
		local maxtime = 7200  -- 120 min (2 hr) * 60 sec
		local multiplier = (maxtime - mintime) / 100 -- Calculates the slider's scaling range that we need

		secondsLeft = timeSlider.value * multiplier + mintime
		clockText.text = convertToDisplayTime(secondsLeft)

		scene:changeImage(foodImage_base)
		
	end

    return true  -- Prevents touch propagation to underlying objects
end

local function resetScene(event)
	--composer.gotoScene("resetScene")
	local currScene = composer.getSceneName( "current" )

	secondsLeft = 10 * 60
	countingDownBoolean = false

	timer.cancel(countDownTimer)
	composer.gotoScene( currScene )
end 

-----------------------------------------------------------------------------------------  
---							MENU OVERLAY/GO TO FUNCTIONS 							  ---
-----------------------------------------------------------------------------------------  

onOptions = function ( self, event )
	local options =
	{
		isModal = true, -- Don't let touches leak through
		effect = "fromTop", -- See list here: http://docs.coronalabs.com/daily/api/library/composer/gotoScene.html
		time = 500,
		params =
		{
			arg1 = "value",
			arg2 = 0
		}
	}
	composer.showOverlay( "recipe_Menu", options  )
	return true
end


goToAvatar = function ()
	composer.gotoScene( "avatar_Menu", { time=800, effect="fromTop" } ) -- crossfade and the time are a transition effect between scenes
end

-- -----------------------------------------------------------------------------------
--									 Scene event functions						   ---
-- -----------------------------------------------------------------------------------

function scene:changeImage( imageName )
	if(imageName == nil)then
		return
	end

	foodImage_base = imageName
	print (foodImage_base)

	local foodImage_path
	
	print (foodImage_path)
	print (timeSlider.value)

	-- Level 1
	if(timeSlider.value < 25 )then
		foodImage_path = foodImage_base .. "_lvl1.png"

	end 

	-- Level 2
	if(timeSlider.value < 50 and timeSlider.value > 25 )then
		foodImage_path = foodImage_base .. "_lvl2.png"

	end 

	-- Level 3
	if(timeSlider.value < 75 and timeSlider.value > 50)then
		foodImage_path = foodImage_base .. "_lvl3.png"
		
	end 

	-- Level 4
	if (timeSlider.value < 100 and timeSlider.value > 75)then
		foodImage_path = foodImage_base .. "_lvl4.png"

	end 

	print (foodImage_path)
	print (timeSlider.value)

	display.remove(pan)

	pan = display.newImageRect( backGroup, foodImage_path, 300, 300)
	pan.x = display.contentCenterX
	pan.y = display.contentCenterY 

end 	

--create: Occurs when the scene is first created but has not yet appeared on screen.
--show:	Occurs twice, immediately before and immediately after the scene appears on screen.
--hide:	Occurs twice, immediately before and immediately after the scene exits the screen.
--destroy:	Occurs if the scene is destroyed.

-- create() composes the scene
-- Here I load up the buttons, background, event listeners
-- We should pause the timer, initialize display groups
function scene:create( event )

	local sceneGroup = self.view
	
	if(width > height) then back.rotation = 90 end
	
	
	-----------------------------------------------------------------------------------------  
	--- 								DISPLAY GROUPS 									  ---
	-----------------------------------------------------------------------------------------  

    -- Set up display groups
	-- Lua uses back to front loading of images/scene stuff so this order is important

	-- Set up display groups
	backGroup = display.newGroup()  -- Display group for the aesthetics ect. bg, set, things that are not interactable
	sceneGroup:insert( backGroup )  -- Insert into the scene's view group

	mainGroup = display.newGroup()  -- Display group for the timer
	sceneGroup:insert( mainGroup )  -- Insert into the scene's view group

	uiGroup = display.newGroup()    -- Display group for UI objects like the button,
	sceneGroup:insert( uiGroup )    -- Insert into the scene's view group

   	-----------------------------------------------------------------------------------------  
	--- 						IMPORT IMAGES / CREATE DISPLAY 							  ---
	-----------------------------------------------------------------------------------------  

	background = display.newImageRect( backGroup, "images/main_menu_ui/background_soft.png", display.contentWidth, display.contentHeight + 100) 
	background.x = centerX
	background.y = centerY

	canvas = display.newImageRect( backGroup, "images/main_menu_ui/canvas.png", display.contentWidth, display.contentHeight + 100) 
	canvas.x = centerX
	canvas.y = centerY

    -- Reference for timer test = https://gist.github.com/coronarob/05accd04ce581b81572f

	pan = display.newImageRect( backGroup, "pan.jpg", 250, 275)
	pan.x = centerX
	pan.y = centerY - 50

	panEgg = display.newImage( backGroup, "panWithEgg.jpg", 200, 225)
	panEgg.x = centerX
	panEgg.y = centerY - 50
	panEgg.alpha = 0

	-- This displays the time and sets the color of the text
	clockText = display.newText( convertToDisplayTime(secondsLeft), display.contentCenterX, 375, native.systemFont, 60)
	clockText:setFillColor(1, 0.5, 0)
	sceneGroup:insert(clockText)


	-----------------------------------------------------------------------------------------   
	--- 									BUTTONS										  --- 
	-----------------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
    -- Pause the timer, at some point the user should be able to input the time
    -- Either with a slider or by clicking a button left/right + 5 min or - 5 min
    -- The time cap should be 2 hours and the min should be 10 min
	--------------------------------------------------------------------------------

	-- The button to start the timer using the widget library
	startButton = widget.newButton(
   	 {
		-- Alignment of the button
		x = centerX,
		y= centerY,
        width = width,
        height = height + 100,
	   
		defaultFile = "images/main_menu_ui/start_button.png",

		-- Event listener to start the timer
		-- onRelease = startTransition
		onPress = startTime
   	 }
	)
	sceneGroup:insert(startButton)

	-- The button to start the timer using the widget library
	pauseButton = widget.newButton(
   	 {
		-- Alignment of the button
		x = centerX,
		y= centerY,
        width = width,
        height = height + 100,
	   
		-- Event listener to start the timer
		onPress = pauseTime
   	 }
	)
	sceneGroup:insert(pauseButton)
	pauseButton.isVisible = false

	-- The button to start the timer using the widget library
	cancelButton = widget.newButton(
		{
		-- Alignment of the button
		x = centerX,
		y= centerY,
		width = width,
		height = height + 100,
		
		defaultFile = "images/main_menu_ui/cancel_button.png",
		-- Event listener to start the timer
		onPress = resetScene
		}
	)
	sceneGroup:insert(cancelButton)
	cancelButton.isVisible = false

	-- The button to start the timer using the widget library
	recipeButton = widget.newButton(
		{
		-- Alignment of the button
		x = 100,
		y= 5,

		-- Properties of the label/text on the button
		label = "Recipe.",
		labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
		fontSize = 20,

		-- Properties for a rounded rectangle button
		shape = "roundedRect",
		width = 100,
		height = 40,
		cornerRadius = 12,
		fillColor = { default={1,0,0,0.5}, over={1,0.1,0.7,0.4} },
		strokeColor = { default={1,0.4,0,1}, over={0.8,0.8,1,1} },
		strokeWidth = 4,

		-- Event listener to start the timer
		onPress = onOptions
		}
	)
	sceneGroup:insert(recipeButton)

	-- The button to start the timer using the widget library
	avatarButton = widget.newButton(
		{
			-- Alignment of the button
			x = 225,
			y= 5,

			-- Properties of the label/text on the button
			label = "Avatar.",
			labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
			fontSize = 20,

			-- Properties for a rounded rectangle button
			shape = "roundedRect",
			width = 100,
			height = 40,
			cornerRadius = 12,
			fillColor = { default={1,0,0,0.5}, over={1,0.1,0.7,0.4} },
			strokeColor = { default={1,0.4,0,1}, over={0.8,0.8,1,1} },
			strokeWidth = 4,

			-- Event listener to start the timer
			onPress = goToAvatar
		}
	)
	sceneGroup:insert(avatarButton)

	-----------------------------------------------------------------------------------------  
	---									 TIME SLIDDER 									  ---
	-----------------------------------------------------------------------------------------  

	-- The slider should increase or decrease the users time
	timeSlider = widget.newSlider(
		{
			x = display.contentCenterX,
			y = 45,
			width = 200,
			-- Start slider at 10% (optional)
			value = 1,  -- Same as timeSlider:setValue( 10 )
			listener = addTime
		}
	)
	sceneGroup:insert(timeSlider)

end

-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

	end
end

-- hide() 
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

		composer.removeScene("main_Menu")
	end
end

-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
