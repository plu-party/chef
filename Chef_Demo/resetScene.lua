-----------------------------------------------------------------------------------------
--
-- resetScene.lua
--
-----------------------------------------------------------------------------------------

--require() command tells Corona to load all of the information about the Composer scene management library into the app.
local composer = require( "composer" )

--  Holds a Composer scene object
local scene = composer.newScene()

-- utilize the Widget Library in this Lua file (similar to importing a class)
local widget = require( "widget" )

local goToMain

-- Function to return to the main menu
goToMain = function ()
	composer.gotoScene( "main_Menu", { time=800, effect="fromTop" } ) -- crossfade and the time are a transition effect between scenes
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
    
    local background = display.newImageRect( "images/main_menu_ui/background_soft.png", display.contentWidth, display.contentHeight + 100)
    background.x = display.contentCenterX
    background.y = display.contentCenterY   

    sceneGroup:insert(background)
    
    -- Cancel button to get out of the overlay using the widget library 
	local cancelButton = widget.newButton(
		{
		-- Alignment of the button
		x = 275,
		y= 10,

		-- Properties of the label/text on the button
		label = "X",
		labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
		fontSize = 22,

		-- Properties for a circle button
		shape = "circle",
		yCenter = 50,
		xCenter = 50,
		radius = 25,
		fillColor = { default={1,0,0,0.5}, over={1,0.1,0.7,0.4} },
		strokeColor = { default={1,0.4,0,1}, over={0.8,0.8,1,1} },
		strokeWidth = 2,

		-- Event listener to start the timer
		onPress = goToMain
		}
	)
    sceneGroup:insert(cancelButton)
    
 -- Function to return to the main menu
    timer.performWithDelay(500, gotoMain, 1 )

    print("in create")
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

print("in show")

	if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        
        print("in will")

    -- Go to the main menu screen (load the main_Menu.lua scene)
    --composer.gotoScene( "main_Menu" )

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

        print("in did")
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end

-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene