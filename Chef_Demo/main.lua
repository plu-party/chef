-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

--require() command tells Corona to load all of the information about the Composer scene management library into the app.
local composer = require( "composer" )
 
-- Hide status bar on the phone
display.setStatusBar( display.HiddenStatusBar )
 
-- Seed the random number generator ( the seed is reset each and every time the app runs )
math.randomseed( os.time() )
 
-- Go to the main menu screen (load the main_Menu.lua scene)
composer.gotoScene( "main_Menu" )