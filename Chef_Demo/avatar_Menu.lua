-- utilize the Composer Library in this Lua file (similar to importing a class)
local composer = require( "composer" )
local scene = composer.newScene()

-- utilize the Widget Library 
local widget = require( "widget" )

-- utilize the Native Library
local native = require( "native" )

-----------------------------------------------------------------------------------------
--- 								LOCAL VARIABLES 								  ---
-----------------------------------------------------------------------------------------

local width = display.contentWidth
local height = display.contentHeight
local centerX = display.contentCenterX
local centerY = display.contentCenterY

-- Tab Display Groups
local tab1Group
local tab2Group
local tab3Group
local tab4Group

-- Functions 
local onBack -- Used to go back to parent
local skintone_Button_Event -- -- Used to change the image on parent
local face_Button_Event
local hair_Button_Event
local clothes_Button_Event
local handleTabBarEvent -- Used to change the tabs 
local goToMain

-- Forward Declarations
local background
local tabBG
local avatarBG
local imageDir
local userName
local userTextField

local skintoneGroup
local faceGroup
local hairGroup
local clothesGroup

-- Default avatar parts
local skintone 

local face

local hair

local clothes

-- Buttons 
local skintone_1, skintone_2, skintone_3
local face_1, face_2, face_3, face_4
local hair_1, hair_2, hair_3, hair_4, hair_5
local clothes_1, clothes_2, clothes_3, clothes_4, clothes_5

-----------------------------------------------------------------------------------------   
--- 							TAB MENU FUNCTIONS  								  ---
-----------------------------------------------------------------------------------------

------- BASE TAB --------
local function base_Menu( sceneGroup )
	
	tabBG =	display.newRect(tab1Group, centerX, centerY + 220, 275, 110 )
	sceneGroup:insert(tabBG)

	skintone_1 = widget.newButton(
		{
			x = display.contentCenterX - 100,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Base/skintone_1.png",
			id = "profile_demo_assets/Base/skintone_1.png", 

			-- Event listener to start the timer
			onPress = skintone_Button_Event
		}
	)		
	sceneGroup:insert(skintone_1)

	skintone_2 = widget.newButton(
		{
			x = display.contentCenterX,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Base/skintone_3.png",
			id = "profile_demo_assets/Base/skintone_3.png", 

			-- Event listener to start the timer
			onPress = skintone_Button_Event
		}
	)		
	sceneGroup:insert(skintone_2)

	skintone_3 = widget.newButton(
		{
			x = display.contentCenterX + 100,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Base/skintone_5.png",
			id = "profile_demo_assets/Base/skintone_5.png", 

			-- Event listener to start the timer
			onPress = skintone_Button_Event
		}
	)		
	sceneGroup:insert(skintone_3)

end 

local function face_Menu( sceneGroup )
	
	tabBG =	display.newRect(tab1Group, centerX, centerY + 220, 275, 110 )
	sceneGroup:insert(tabBG)

	face_1 = widget.newButton(
		{
			x = display.contentCenterX - 100,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Face/face_1.png",
			id = "profile_demo_assets/Face/face_1.png", 

			-- Event listener to start the timer
			onPress = face_Button_Event,
		}
	)		
	sceneGroup:insert(face_1)

	face_2 = widget.newButton(
		{
			x = display.contentCenterX - 25,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Face/face_2.png",
			id = "profile_demo_assets/Face/face_2.png", 

			-- Event listener to start the timer
			onPress = face_Button_Event,
		}
	)		
	sceneGroup:insert(face_2)

	face_3 = widget.newButton(
		{
			x = display.contentCenterX + 50,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Face/face_3.png",
			id = "profile_demo_assets/Face/face_3.png", 

			-- Event listener to start the timer
			onPress = face_Button_Event,
		}
	)		
	sceneGroup:insert(face_3)

	face_4 = widget.newButton(
		{
			x = display.contentCenterX + 125 ,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Face/face_4.png",
			id = "profile_demo_assets/Face/face_4.png", 

			-- Event listener to start the timer
			onPress = face_Button_Event,
		}
	)		
	sceneGroup:insert(face_4)

end 

local function hair_Menu( sceneGroup )
	
	tabBG =	display.newRect(tab1Group, centerX, centerY + 220, 275, 110 )
	sceneGroup:insert(tabBG)

	hair_1 = widget.newButton(
		{
			x = display.contentCenterX - 100,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Hair/hair_1.png",
			id = "profile_demo_assets/Hair/hair_1.png", 

			-- Event listener to start the timer
			onPress = hair_Button_Event,
		}
	)		
	sceneGroup:insert(hair_1)

	hair_2 = widget.newButton(
		{
			x = display.contentCenterX - 50,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Hair/hair_2.png",
			id = "profile_demo_assets/Hair/hair_2.png", 

			-- Event listener to start the timer
			onPress = hair_Button_Event,onPress = hair_Button_Event,
		}
	)		
	sceneGroup:insert(hair_2)

	hair_3 = widget.newButton(
		{
			x = display.contentCenterX ,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Hair/hair_3.png",
			id = "profile_demo_assets/Hair/hair_3.png", 

			-- Event listener to start the timer
			onPress = hair_Button_Event,
		}
	)		
	sceneGroup:insert(hair_3)

	hair_4 = widget.newButton(
		{
			x = display.contentCenterX + 50 ,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Hair/hair_4.png",
			id = "profile_demo_assets/Hair/hair_4.png", 

			-- Event listener to start the timer
			onPress = hair_Button_Event,
		}
	)		
	sceneGroup:insert(hair_4)

	hair_5 = widget.newButton(
		{
			x = display.contentCenterX + 100 ,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Hair/hair_5.png",
			id = "profile_demo_assets/Hair/hair_5.png", 

			-- Event listener to start the timer
			onPress = hair_Button_Event,
		}
	)		
	sceneGroup:insert(hair_5)

end 

local function clothes_Menu( sceneGroup )
	
	tabBG =	display.newRect(tab1Group, centerX, centerY + 220, 275, 110 )
	sceneGroup:insert(tabBG)

	clothes_1 = widget.newButton(
		{
			x = display.contentCenterX - 100,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Clothes/1.png",
			id = "profile_demo_assets/Clothes/1.png", 

			-- Event listener to start the timer
			onPress = clothes_Button_Event,
		}
	)		
	sceneGroup:insert(clothes_1)

	clothes_2 = widget.newButton(
		{
			x = display.contentCenterX - 50,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Clothes/3.png",
			id = "profile_demo_assets/Clothes/3.png", 

			-- Event listener to start the timer
			onPress = clothes_Button_Event,
		}
	)		
	sceneGroup:insert(clothes_2)

	clothes_3 = widget.newButton(
		{
			x = display.contentCenterX ,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Clothes/8.png",
			id = "profile_demo_assets/Clothes/8.png", 

			-- Event listener to start the timer
			onPress = clothes_Button_Event,
		}
	)		
	sceneGroup:insert(clothes_3)

	clothes_4 = widget.newButton(
		{
			x = display.contentCenterX + 50 ,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Clothes/13.png",
			id = "profile_demo_assets/Clothes/13.png", 

			-- Event listener to start the timer
			onPress = clothes_Button_Event,
		}
	)		
	sceneGroup:insert(clothes_4)

	clothes_5 = widget.newButton(
		{
			x = display.contentCenterX + 100 ,
			y = display.contentCenterY + 220,
			width = 120,
			height = 120,
			defaultFile = "profile_demo_assets/Clothes/11.png",
			id = "profile_demo_assets/Clothes/11.png", 

			-- Event listener to start the timer
			onPress = clothes_Button_Event,
		}
	)		
	sceneGroup:insert(clothes_5)

end 

local function load_TabMenu ( tab_Menu )

	if (tab_Menu == "tab1") then
		tab1Group.isVisible = true

	else 
		tab1Group.isVisible = false
	end

	if (tab_Menu == "tab2") then
		tab2Group.isVisible = true

	else 
		tab2Group.isVisible = false
	end

	if (tab_Menu == "tab3") then
		tab3Group.isVisible = true

	else 
		tab3Group.isVisible = false
	end

	if (tab_Menu == "tab4") then
		tab4Group.isVisible = true

	else 
		tab4Group.isVisible = false
	end

end

----------------------------------------------------------------------
--				FUNCTION/CALLBACK DEFINITIONS						--
----------------------------------------------------------------------

-- Function to handle button events
skintone_Button_Event = function ( event )

	imageDir = event.target.id
	print(imageDir)

	display.remove(skintone)

	skintone = display.newImageRect( imageDir, 250, 250 )
	skintone.x = centerX
	skintone.y = centerY - 50

	skintoneGroup:insert(skintone)

end

-- Function to handle button events
face_Button_Event = function ( event )

	imageDir = event.target.id
	print(imageDir)

	display.remove(face)

	face = display.newImageRect( imageDir, 250, 250 )
	face.x = centerX
	face.y = centerY - 50

	faceGroup:insert(face)

end

-- Function to handle button events
hair_Button_Event = function ( event )

	imageDir = event.target.id
	print(imageDir)

	display.remove(hair)

	hair = display.newImageRect( imageDir, 250, 250 )
	hair.x = centerX
	hair.y = centerY - 50

	hairGroup:insert(hair)

end

-- Function to handle button events
clothes_Button_Event = function ( event )

	imageDir = event.target.id
	print(imageDir)

	display.remove(clothes)

	clothes = display.newImageRect( imageDir, 250, 250 )
	clothes.x = centerX
	clothes.y = centerY - 50

	clothesGroup:insert(clothes)

end

-- Function to handle tab events
handleTabBarEvent = function ( event )
	print( event.target.id )  -- Reference to button's 'id' parameter

	load_TabMenu(event.target.id)
end

-- Function to return to the main menu
goToMain = function ()
	composer.gotoScene( "main_Menu", { time=800, effect="fromTop" } ) -- crossfade and the time are a transition effect between scenes
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )
	local sceneGroup = self.view
	
	if(width > height) then back.rotation = 90 end

	-- Set up display groups
	tab1Group = display.newGroup()  -- Display group for the aesthetics ect. bg, set, things that are not interactable
	tab2Group = display.newGroup()
	tab3Group = display.newGroup()
	tab4Group = display.newGroup()

	skintoneGroup = display.newGroup()
	faceGroup = display.newGroup()
	hairGroup = display.newGroup()
	clothesGroup = display.newGroup()

	background = display.setDefault("background", 0.99, 0.99, 0.59 ) --background color

	avatarBG = display.newRect(sceneGroup, centerX, centerY - 50, 250, 300)

	-- Default Avatar
	skintone = display.newImageRect( "profile_demo_assets/Base/skintone_3.png", 250, 250)
	skintone.x = centerX
	skintone.y = centerY - 50
	skintoneGroup:insert(skintone)

	face = display.newImageRect ( "profile_demo_assets/Face/face_1.png", 250, 250 ) 
	face.x = centerX
	face.y = centerY - 50
	faceGroup:insert(face)

	hair = display.newImageRect ( "profile_demo_assets/Hair/hair_3.png", 250, 250 )
	hair.x = centerX
	hair.y = centerY - 50
	hairGroup:insert(hair)

	clothes = display.newImageRect( "profile_demo_assets/Clothes/8.png", 250, 250 )
	clothes.x = centerX
	clothes.y = centerY - 50
	clothesGroup:insert(clothes)

	-----------------------------------------------------------------------------------------   
	--- 									BUTTONS 									  ---
	-----------------------------------------------------------------------------------------

	-- Cancel button to get out of the overlay using the widget library 
	local cancelButton = widget.newButton(
		{
		-- Alignment of the button
		x = 275,
		y= 10,

		-- Properties of the label/text on the button
		label = "X",
		labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
		fontSize = 22,

		-- Properties for a circle button
		shape = "circle",
		yCenter = 50,
		xCenter = 50,
		radius = 25,
		fillColor = { default={1,0,0,0.5}, over={1,0.1,0.7,0.4} },
		strokeColor = { default={1,0.4,0,1}, over={0.8,0.8,1,1} },
		strokeWidth = 2,

		-- Event listener to start the timer
		onPress = goToMain
		}
	)
	sceneGroup:insert(cancelButton)

	-----------------------------------------------------------------------------------------
	--- 									TAB BAR 									  --- 
	-----------------------------------------------------------------------------------------

	-- Make sure tab1 is the first menu that shows up
	load_TabMenu("tab1")

	-- Configure the tab buttons to appear within the bar
	local tabButtons = {
		{
			label = "Base",
			id = "tab1",
			selected = true,
			size = 22,
			labelYOffset = -15,
			onPress = handleTabBarEvent
		},
		{
			label = "Face",
			id = "tab2",
			size = 22,
			labelYOffset = -15,
			onPress = handleTabBarEvent
		},
		{
			label = "Hair",
			id = "tab3",
			size = 22,
			labelYOffset = -15,
			onPress = handleTabBarEvent
		},
		{
			label = "Clothes",
			id = "tab4",
			size = 22,
			labelYOffset = -15,
			onPress = handleTabBarEvent
		}
	}
	
	-- Create the widget
	local tabBar = widget.newTabBar(
		{
			top = 350,
			left = 10,
			width = 300,
			buttons = tabButtons
		}
	)
	sceneGroup:insert(tabBar)

	-- Insert the tab groups into the scene and call the tab menus 
	sceneGroup:insert(tab1Group)
	base_Menu(tab1Group)

	sceneGroup:insert(tab2Group)
	face_Menu(tab2Group)

	sceneGroup:insert(tab3Group)
	hair_Menu(tab3Group)

	sceneGroup:insert(tab4Group)
	clothes_Menu(tab4Group)

	sceneGroup:insert(skintoneGroup)

	sceneGroup:insert(faceGroup)

	sceneGroup:insert(hairGroup)

	sceneGroup:insert(clothesGroup)

end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
