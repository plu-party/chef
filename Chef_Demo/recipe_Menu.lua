-----------------------------------------------------------------------------------------
--
-- recipe_Menu.lua
-- Here we load/create the recipe repository 
--
-----------------------------------------------------------------------------------------

-- utilize the Composer library in this Lua file (must be used in every Composer scene)
local composer = require( "composer" )

--  Holds a Composer scene object
local scene = composer.newScene()

-- utilize the Widget Library in this Lua file (similar to importing a class)
local widget = require( "widget" )

-----------------------------------------------------------------------------------------
--- 								LOCAL VARIABLES 								  ---
-----------------------------------------------------------------------------------------

local width = display.contentWidth
local height = display.contentHeight
local centerX = display.contentCenterX
local centerY = display.contentCenterY

-- Tab Display Groups
local tab1Group
local tab2Group
local tab3Group

-- Functions 
local onBack -- Used to go back to parent
local image_Button_Event -- -- Used to change the image on parent
local handleTabBarEvent -- Used to change the tabs 

-- Forward Declarations
local background
local transperancy 
local tabBG
local imageDir

-- Food Declarations
local boba_button, cookie_button, khaoSoi_button, mochi_button, noodle_button, 
onigiri_button, pancake_button, pb_j_button, rice_curry_button, toast_button

-----------------------------------------------------------------------------------------   
--- 							TAB MENU FUNCTIONS  								  ---
-----------------------------------------------------------------------------------------

-- This is the menu when you are in tab 1 
local function tab1_Menu( sceneGroup )
	
	tabBG =	display.newRect(160, 260, 240, 285 )
	sceneGroup:insert(tabBG)

	--- Selectable items (I assume they will act as buttons)

		-- Boba button
		boba_button = widget.newButton(
			{
				x = display.contentCenterX - 60,
				y = display.contentCenterY - 50,
				width = 120,
				height = 120,
				defaultFile = "images/boba.png",
				id = "images/boba.png", 

				label = "Boba, yum!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
		
			}
		)		
		sceneGroup:insert(boba_button)

		-- Cookie button
		cookie_button = widget.newButton(
			{
				x = display.contentCenterX + 60,
				y = display.contentCenterY - 50,
				width = 120,
				height = 120,
				defaultFile = "images/cookie.png",
				id = "images/cookie.png",

				label = "Cookies!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)
		sceneGroup:insert(cookie_button)

		-- Khao Soi button
		khaoSoi_button = widget.newButton(
			{
				x = display.contentCenterX - 60,
				y = display.contentCenterY + 80,
				width = 120,
				height = 120,
				defaultFile = "images/KhaoSoi.png",
				id = "images/KhaoSoi.png",

				label = "Khao Soi!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)		
		sceneGroup:insert(khaoSoi_button)

		-- Mochi button
		mochi_button = widget.newButton(
			{
				x = display.contentCenterX + 60,
				y = display.contentCenterY + 80,
				width = 120,
				height = 120,
				defaultFile = "images/mochi.png",
				id = "images/mochi.png",

				label = "Mochi!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)
		sceneGroup:insert(mochi_button)

end 

-- This is the menu when you are in tab 2
local function tab2_Menu( sceneGroup )
	
	tabBG =	display.newRect(160, 260, 240, 285 )
	sceneGroup:insert(tabBG)

	--- Selectable items (I assume they will act as buttons)

		-- Noodle button
		noodle_button = widget.newButton(
			{
				x = display.contentCenterX - 60,
				y = display.contentCenterY - 50,
				width = 120,
				height = 120,
				defaultFile = "images/noodle.png",
				id = "images/noodle.png",

				label = "Noodle!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)		
		sceneGroup:insert(noodle_button)

		-- Onigiri button
		onigiri_button = widget.newButton(
			{
				x = display.contentCenterX + 60,
				y = display.contentCenterY - 50,
				width = 120,
				height = 120,
				defaultFile = "images/onigiri.png",
				id = "images/onigiri.png",

				label = "Onigiri!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)
		sceneGroup:insert(onigiri_button)

		-- Pancake button
		pancake_button = widget.newButton(
			{
				x = display.contentCenterX - 60,
				y = display.contentCenterY + 80,
				width = 120,
				height = 120,
				defaultFile = "images/pancake.png",
				id = "images/pancake.png",

				label = "Pancakes!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)		
		sceneGroup:insert(pancake_button)

		-- PB&J button
		pb_j_button = widget.newButton(
			{
				x = display.contentCenterX + 60,
				y = display.contentCenterY + 80,
				width = 120,
				height = 120,
				defaultFile = "images/pb&j.png",
				id =  "images/pb&j.png",

				label = "PB & J!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)
		sceneGroup:insert(pb_j_button)
end 

local function tab3_Menu( sceneGroup )
	
	tabBG =	display.newRect(160, 260, 240, 285 )
	sceneGroup:insert(tabBG)

	--- Selectable items (I assume they will act as buttons)

		-- Rice and curry button
		rice_curry_button = widget.newButton(
			{
				x = display.contentCenterX - 60,
				y = display.contentCenterY - 50,
				width = 120,
				height = 120,
				defaultFile = "images/rice&curry.png",
				id = "images/rice&curry.png",

				label = "CURRY!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)		
		sceneGroup:insert(rice_curry_button)

		-- Toast button
		toast_button = widget.newButton(
			{
				x = display.contentCenterX + 60,
				y = display.contentCenterY - 50,
				width = 200,
				height = 200,
				defaultFile = "images/food assets/toast_lvl1.png",
				id = "images/food assets/toast",

				label = "Toast!", -- Same as startButton:setLabel( "Let's cook!" )
				labelColor = { default={ 0.13, 0.6, 0.7 } },
				fontSize = 20,
				labelYOffset =  65,

				-- Event listener to start the timer
				onPress = image_Button_Event
			}
		)
		sceneGroup:insert(toast_button)

end 

local function load_TabMenu ( tab_Menu )

	if (tab_Menu == "tab1") then
		tab1Group.isVisible = true

	else 
		tab1Group.isVisible = false
	end

	if (tab_Menu == "tab2") then
		tab2Group.isVisible = true

	else 
		tab2Group.isVisible = false
	end

	if (tab_Menu == "tab3") then
		tab3Group.isVisible = true

	else 
		tab3Group.isVisible = false
	end

end

----------------------------------------------------------------------
--				CALLBACK/FUNCTIONS DEFINITIONS						--
----------------------------------------------------------------------
onBack = function ( self, event ) 
	composer.hideOverlay( "slideUp", 500  )	
	return true
end

-- Function to handle button events
image_Button_Event = function ( event )
	imageDir = event.target.id
	composer.hideOverlay( "slideUp", 500 )
end

-- Function to handle button events
handleTabBarEvent = function ( event )
	print( event.target.id )  -- Reference to button's 'id' parameter

	load_TabMenu(event.target.id)
end

-- -----------------------------------------------------------------------------------
-- 								Scene event functions							   ---
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view

	if(width > height) then back.rotation = 90 end

	-- Set up display groups
	tab1Group = display.newGroup()  -- Display group for the aesthetics ect. bg, set, things that are not interactable
	tab2Group = display.newGroup()
	tab3Group = display.newGroup()

	-- Code here runs when the scene is first created but has not yet appeared on screen

	-- Create a rectangle to force the opacity of the parent scene down
	transperancy = display.newRect( sceneGroup, centerX, centerY, 100000, 100000 )
	transperancy:setFillColor( 1.0, 0.78, 0.5 )
	transperancy.alpha = 0.5

    -- Create a simple background
	background = display.newRoundedRect( sceneGroup, centerX, centerY, 275, 375, 30 )
	background:setFillColor( 1.0, 0.58, 0.32 )

	-----------------------------------------------------------------------------------------   
	--- 									BUTTONS 									  ---
	-----------------------------------------------------------------------------------------

	-- Cancel button to get out of the overlay using the widget library 
	local cancelButton = widget.newButton(
		{
		-- Alignment of the button
		x = 275,
		y= 50,

		-- Properties of the label/text on the button
		label = "X",
		labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
		fontSize = 22,

		-- Properties for a circle button
		shape = "circle",
		yCenter = 50,
		xCenter = 50,
		radius = 25,
		fillColor = { default={1,0,0,0.5}, over={1,0.1,0.7,0.4} },
		strokeColor = { default={1,0.4,0,1}, over={0.8,0.8,1,1} },
		strokeWidth = 2,

		-- Event listener to start the timer
		onPress = onBack
		}
	)
	sceneGroup:insert(cancelButton)

	-----------------------------------------------------------------------------------------
	--- 									TAB BAR 									  --- 
	-----------------------------------------------------------------------------------------

	-- Make sure tab1 is the first menu that shows up
	load_TabMenu("tab1")

	-- Configure the tab buttons to appear within the bar
	local tabButtons = {
		{
			label = "Tab1",
			id = "tab1",
			selected = true,
			size = 22,
			labelYOffset = -8,
			onPress = handleTabBarEvent
		},
		{
			label = "Tab2",
			id = "tab2",
			size = 22,
			labelYOffset = -8,
			onPress = handleTabBarEvent
		},
		{
			label = "Tab3",
			id = "tab3",
			size = 22,
			labelYOffset = -8,
			onPress = handleTabBarEvent
		}
	}
	
	-- Create the widget
	local tabBar = widget.newTabBar(
		{
			top = 75,
			left = 60,
			width = 200,
			buttons = tabButtons
		}
	)
	sceneGroup:insert(tabBar)

	-- Insert the tab groups into the scene and call the tab menus 
	sceneGroup:insert(tab1Group)
	tab1_Menu(tab1Group)

	sceneGroup:insert(tab2Group)
	tab2_Menu(tab2Group)

	sceneGroup:insert(tab3Group)
	tab3_Menu(tab3Group)

end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	local parent = event.parent  -- Reference to the parent scene object

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
		
		--event.parent.pan = "images/pancake.png"

		event.parent:changeImage(imageDir)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end		

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
---------------------------------------------------------------------------------
return scene