# chef

## Problem Statement
It is easy to get side tracked by your phone.  What if we had a solution that allowed us to stay focused and also be rewarded**

** reward being both personal but also in the future contribute to community outreach

## Product Foundation
The product MVP should be a time tracker that allows a user to set up a character and start creating recipes based on time intervals. A concept of reaching or not reaching the time interval will also be created using positive and negative reviews.  Exporting data and providing a feedback loop will be basic to start.

## Minimum Viable Product
1. Track time functionality (time intervals)
2. Allow user to create a profile with a minimum set of characteristics (character, emotion, reviews)
3. Landing page which includes a timer and recipe (MVP: Instant Raman)
4. Recipe repository (list of all recipes by time limits) 
5. Repository of booked items (what recipes have been created / archive)
6. Ability to export portfolio & experience (time, date, outcome, badging, negative outcomes)
3. Allow multiple tags on time intervals (what time tracker was used for)


## Getting Started

### clone
```git clone https://gitlab.com/plu-party/chef.git```

### test
```TBD, THERE'S NO CODE HERE YET!```

### build/run
```TBD, THERE'S NO CODE HERE YET!```


## Contributing

### Contributors

Max Staples
Lauren Cannon

### discord
[Discord Server](https://discord.gg/kkbESMB)

### work tracking
[GitLab Built-in Issues/Boards](https://gitlab.com/plu-party/chef/-/boards)

### design
[Google Drive - Chef](https://drive.google.com/drive/folders/1U1fTWsQdpKCqfPYSz6_EiprIv8XZR4Wm?usp=sharing)

### branch
```git checkout -b <issue-num-short-description>```

### commit
```git add <files to add>```
```git commit -m "description of the commit"```
```git push```

### merge requests
Use the gitlab UI to create a merge request of your branch into master
[How to create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

### review
Pick a team member along with the technical advisor and assign them to approve your merge request

### merge
Once your merge request is approved, merge your code into master
